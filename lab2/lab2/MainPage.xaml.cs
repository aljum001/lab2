﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace lab2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        public async void B1(Button Sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1());
            
        }

        public async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(500);
            b.BackgroundColor = Color.White;
        }

        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            b.BackgroundColor = Color.NavajoWhite;
        }

        public async void C_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }
    }
}
